/** Rutas de navegacion de la App */
enum Pages {
  LOGINPAGE = "LoginPage",
  HOMEPAGE = "HomePage",
  SEARCHPAGE = "SearchPage",
}

export { Pages };
