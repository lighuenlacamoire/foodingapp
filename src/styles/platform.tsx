/**
 * Estilos genericos
 */
const platform = {
  spaceSizes: {
    horizontal: 16,
  },
  fontSizes: {
    SMALL: 12,
    MEDIUM: 14,
    LARGE: 16,
    HEADER: 18,
    TITLE: 20,
    ICON: 20,
  },
  colors: {
    primary: "#008f7e",
    secondary: "#D4F9F5",
    title: "#333333",
    placeholder: "#CCCCCC",
    notes: "#ADADAD",
    default: "#F2F2F2",
    white: "#FFF",
  },
};

export default platform;
