import { Pages } from "../configuration/constants";

export type NavigationParamList = {
  [Pages.LOGINPAGE]: undefined;
  [Pages.HOMEPAGE]: undefined;
  [Pages.SEARCHPAGE]: undefined;
};
