import {
  NavigationContainer,
  useNavigationContainerRef,
} from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
//import { useSelector } from 'react-redux';
import { Pages } from "../configuration/constants";
import { HomePage } from "../pages/Home";
import { SearchPage } from "../pages/Search";
//import { RootState } from '../redux/store';

/**
 * Nivel de navegacion inicial
 */
const RootRouter = (): JSX.Element => {
  const routeNameRef = React.useRef<string>();
  const navigationRef = useNavigationContainerRef(); // You can also use a regular ref with `React.useRef()`
  const RootStack = createStackNavigator();
  //const { user } = useSelector((state: RootState) => state.auth);

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => {
        routeNameRef.current = navigationRef.current?.getCurrentRoute()?.name;
      }}
      onStateChange={async () => {
        // const previousRouteName = routeNameRef.current;
        const currentRouteName = navigationRef.current?.getCurrentRoute()?.name;

        routeNameRef.current = currentRouteName;
      }}>
      <RootStack.Navigator>
        <RootStack.Screen
          name={Pages.HOMEPAGE}
          component={HomePage}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name={Pages.SEARCHPAGE}
          component={SearchPage}
          options={{ headerShown: false }}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default RootRouter;
