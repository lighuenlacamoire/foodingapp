import { useNavigation } from "@react-navigation/native";
import React from "react";
import {
  View,
  Text,
  ScrollView,
  useWindowDimensions,
  TouchableOpacity,
} from "react-native";
import { Icon } from "../../components/Icon";
import { CelularHand } from "../../components/Images";
import { Pages } from "../../configuration/constants";
import platform from "../../styles/platform";

const HomePage = () => {
  const { height } = useWindowDimensions();
  /** Navegacion */
  const navigation = useNavigation();
  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          //backgroundColor: "red",
          flexDirection: "row",
          justifyContent: "space-between",
          paddingVertical: 4,
          paddingHorizontal: platform.spaceSizes.horizontal,
        }}>
        <View
          style={{
            backgroundColor: "white",
            height: 40,
            width: 40,
            borderRadius: 20,
            justifyContent: "center",
            alignItems: "center",
            borderWidth: 2,
            borderColor: platform.colors.primary,
          }}>
          <Text>aa</Text>
        </View>
        <View>
          <Icon name="search" size={24} color="#008f7e" />
        </View>
      </View>
      <View
        style={{
          height: height / 3,
          //backgroundColor: "red",
          marginBottom: -16,
          flexDirection: "row",
          paddingHorizontal: platform.spaceSizes.horizontal,
        }}>
        <View style={{ flex: 1, justifyContent: "center" }}>
          <Text
            style={{
              fontSize: 42,
              color: platform.colors.title,
              fontWeight: "700",
            }}>
            Tenpo
          </Text>
          <Text
            style={{
              fontSize: 42,
              color: platform.colors.primary,
              fontWeight: "700",
            }}>
            Eats
          </Text>
          <Text
            style={{
              fontSize: platform.fontSizes.SMALL,
              color: platform.colors.title,
              textTransform: "uppercase",
              fontWeight: "700",
            }}>
            Deliver app
          </Text>
        </View>
        <View
          style={{ flex: 1, justifyContent: "flex-end", alignItems: "center" }}>
          <CelularHand />
        </View>
      </View>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        contentContainerStyle={{ flexGrow: 1 }}>
        <View
          style={{
            flex: 1,
            backgroundColor: platform.colors.secondary,
            borderTopLeftRadius: 28,
            borderTopRightRadius: 28,
          }}>
          <View
            style={{
              alignItems: "center",
              paddingVertical: platform.spaceSizes.horizontal + 2,
            }}>
            <TouchableOpacity
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
              onPress={() => navigation.navigate(Pages.SEARCHPAGE)}>
              <Icon
                name="map-marked-alt"
                size={16}
                color={platform.colors.primary}
              />
              <Text
                style={{
                  marginLeft: 8,
                  fontSize: platform.fontSizes.LARGE,
                  fontWeight: "300",
                  color: platform.colors.primary,
                }}>
                Agregar dirección de entrega
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              backgroundColor: platform.colors.white,
              borderTopLeftRadius: 28,
              borderTopRightRadius: 28,
              paddingHorizontal: platform.spaceSizes.horizontal,
              flex: 1,
              paddingTop: 38,
            }}>
            <Text
              style={{
                fontWeight: "700",
                fontSize: platform.fontSizes.HEADER,
                color: platform.colors.title,
              }}>
              RESTAURANTES
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default HomePage;
