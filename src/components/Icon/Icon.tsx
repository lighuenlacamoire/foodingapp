import React from "react";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

type Props = {
  name: string;
  size?: number;
  color?: string;
};

/**
 * Icono generico
 */
const Icon = ({ name, size, color }: Props) => {
  return <FontAwesome5 name={name} color={color} size={size} light />;
};

export default Icon;
