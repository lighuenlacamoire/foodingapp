module.exports = {
  arrowParens: 'always', // agregar los parentesis en una arrow function cuando se escribe un solo parametro
  bracketSameLine: true,
  bracketSpacing: true, // agregar espacios al momento de importar una dependencia entre llaves
  printWidth: 80,
  semi: true, // agregar punto y coma
  singleQuote: false, // cambiar las comillas dobles por simples
  trailingComma: 'all',
};
