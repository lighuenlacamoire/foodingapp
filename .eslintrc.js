module.exports = {
  root: true,
  env: {
    node: true,
    jest: true,
    es2022: true,
  },
  extends: [
    "eslint:recommended",
    "plugin:import/typescript",
    "plugin:react/recommended",
    "plugin:prettier/recommended",
    "plugin:@typescript-eslint/recommended",
  ],
  settings: {
    "import/resolver": {
      "babel-module": {},
      "node": {
        "extensions": [".js", ".jsx", ".ts", ".tsx", ".d.ts"]
      }
    }
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: "latest",
    sourceType: "module",
    project: "./tsconfig.json",
    tsconfigRootDir: "./",
  },
  plugins: ["react", "@typescript-eslint", "prettier"],
  ignorePatterns: [
    ".eslintrc.js"
  ],
  rules: {
    "linebreak-style": ["error", "unix"],
    "@typescript-eslint/strict-boolean-expressions": "off",
    "@typescript-eslint/no-floating-promises": "off",
  },
};
