/* eslint-disable @typescript-eslint/no-empty-interface */
import { NavigationParamList } from "./src/interfaces/navigations";

declare global {
  namespace ReactNavigation {
    interface RootParamList extends NavigationParamList {}
  }
}
