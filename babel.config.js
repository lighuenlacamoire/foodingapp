module.exports = {
  presets: ["module:metro-react-native-babel-preset"],
  plugins: [
    [
      "module-resolver",
      {
        extensions: [".ios.js", ".android.js", ".js", ".ts", ".tsx", ".json"],
        alias: {
          "@components": "./src/components",
          "@config": "./src/configuration",
          "@styles": "./src/styles",
          "@redux": "./src/redux",
          "@pages": "./src/pages",
          "@interfaces": "./src/interfaces",
          "@services": "./src/services",
          "@utils": "./src/utils",
        },
      },
    ],
  ],
};
